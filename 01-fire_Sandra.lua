--[[ [previous](00-contents.lua) | [contents](00-contents.lua) | [next](02-growing-society.lua) ]]

FireSandra = Model{
    finalTime = 100, --Intervalo máximo de tempo NSTEPS
    dim = 201, -- Tamanho da grade
    D = 0.6, --Proporção de células com vegetação
    B = 0.1, --Probabilidade de uma celula mudar seu estado de queimando para queimada
    I = 0.3, --Probabilidade de uma celula em chamas propagar para uma célula de vegetação vizinha

    init = function(model)
		model.cell = Cell{

			init = function(cell)
				if Random():number() < model.D then
					cell.state = "forest"
				else
					cell.state = "empty"
				end
			end,
			execute = function(cell)
				if cell.past.state == "burning" then
                    if Random():number() > model.B then
                        cell.state = "burned"
                    end
				elseif cell.past.state == "forest" then
                    forEachNeighbor(cell, function(neigh)
                        if neigh.past.state == "burning" then
                            if Random():number() > model.I then
                                cell.state = "burning"
                            end
                        end
                    end)
				end
			end
		}

        model.cs = CellularSpace{
            xdim = model.dim,
            instance = model.cell,
        }

        model.cs:createNeighborhood{strategy = "moore"}
--        model.cs:sample().state = "burning"
        model.cs:get(100, 100).state = "burning"

        model.chart = Chart{
            target = model.cs,
            select = "state",
            value = {"forest", "burning", "burned"},
            color = {"green", "red", "gray"}
        }

        model.map = Map{
            target = model.cs,
            select = "state",
			value = {"forest", "burning", "burned", "empty"},
			color = {"green", "red", "gray", "black"}
        }

        model.timer = Timer{
            Event{action = model.cs},
            Event{action = model.chart},
            Event{action = model.map}
        }
    end
}

fire = FireSandra{}
fire:run()

